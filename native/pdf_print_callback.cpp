#include "pdf_print_callback.h"

#include "jni_util.h"
#include "util.h"

PdfPrintCallback::PdfPrintCallback(JNIEnv* env, jobject jcallback) {
  jcallback_ = env->NewGlobalRef(jcallback);
}
PdfPrintCallback::~PdfPrintCallback() {
  JNIEnv* env = GetJNIEnv();
  env->DeleteGlobalRef(jcallback_);
}

void PdfPrintCallback::OnPdfPrintFinished(const CefString& path, bool ok) {
  if (!jcallback_)
    return;

  JNIEnv* env = GetJNIEnv();
  if (!env)
    return;

  JNI_CALL_VOID_METHOD(env, jcallback_,
    "OnPdfPrintFinished",
    "(Ljava/lang/String;Z)V",
    NewJNIString(env, path), (jboolean)ok);
}