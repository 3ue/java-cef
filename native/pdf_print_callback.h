// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#ifndef JCEF_NATIVE_PDF_RPINT_CALLBACK_H_
#define JCEF_NATIVE_PDF_RPINT_CALLBACK_H_

#include <jni.h>
#include "include/cef_browser.h"

class PdfPrintCallback : public CefPdfPrintCallback {

public:
  PdfPrintCallback(JNIEnv* env, jobject jcallback);
  virtual ~PdfPrintCallback();

  virtual void OnPdfPrintFinished(const CefString& path, bool ok) OVERRIDE;

protected:
  jobject jcallback_;

  // Include the default reference counting implementation.
  IMPLEMENT_REFCOUNTING(PdfPrintCallback);
};

#endif

