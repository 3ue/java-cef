// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#include "CefPdfPrintSettings_N.h"
#include "jni_util.h"

#define UNKNOWN_VALUE -1

// Set the CEF base object for an existing JNI object. A reference will be
// added to the base object. If a previous base object existed a reference
// will be removed from that object.
template <class T>
bool SetCefRawPointerForJNIObject(JNIEnv* env, jobject obj, T* base, const char* varName) {
  jstring identifer = env->NewStringUTF(varName);
  jlong previousValue = 0;
  if (!obj)
    return false;
  JNI_CALL_METHOD(env, obj, "getNativeRef", "(Ljava/lang/String;)J", Long, previousValue, identifer);

  T* oldbase = reinterpret_cast<T*>(previousValue);
  if (oldbase) {
    delete oldbase;
  }

  JNI_CALL_VOID_METHOD(env, obj, "setNativeRef", "(Ljava/lang/String;J)V", identifer, (jlong)base);
  return true;
}

cef_string_t GetJNIString1(JNIEnv* env, jstring jstr)
{
  CefString str = GetJNIString(env, jstr);
  cef_string_t myCefAgent(*(str.GetWritableStruct()));
  return myCefAgent;
}

JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1CefPdfPrintSettings_1CTOR
    (JNIEnv *env, jobject obj) {
  CefPdfPrintSettings* settings = new CefPdfPrintSettings();
  if (!settings)
    return;
  SetCefRawPointerForJNIObject(env, obj, settings, "CefPdfPrintSettings");
}

JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1CefPdfPrintSettings_1DTOR
(JNIEnv *env, jobject obj) {
  CefPdfPrintSettings* settings = NULL;
  SetCefRawPointerForJNIObject(env, obj, settings, "CefPdfPrintSettings");
}

JNIEXPORT jstring JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1GetHeaderFooterTitle
(JNIEnv *env, jobject obj) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return NULL;
  return NewJNIString(env, settings->header_footer_title);
}


JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1SetHeaderFooterTitle
(JNIEnv *env, jobject obj, jstring str) {
  cef_string_t stri = GetJNIString1(env, str);
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (settings) {
    settings->header_footer_title = stri;
  }
}

JNIEXPORT jstring JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1GetHeaderFooterUrl
(JNIEnv *env, jobject obj) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return NULL;
  return NewJNIString(env, settings->header_footer_url);
}

JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1SetHeaderFooterUrl
(JNIEnv *env, jobject obj, jstring jstr) {
  cef_string_t stri = GetJNIString1(env, jstr);
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (settings) {
    settings->header_footer_url = stri;
  }
}

JNIEXPORT jint JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1GetPageWidth
(JNIEnv *env, jobject obj) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return UNKNOWN_VALUE;
  return (jint)settings->page_width;
}

/*
* Class:     org_cef_misc_CefPdfPrintSettings_N
* Method:    N_SetPageWidth
* Signature: (I)V
*/
JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1SetPageWidth
(JNIEnv * env, jobject obj, jint page_width) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return;
  settings->page_width = page_width;
}

/*
* Class:     org_cef_misc_CefPdfPrintSettings_N
* Method:    N_GetPageHeigth
* Signature: ()I
*/
JNIEXPORT jint JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1GetPageHeigth
(JNIEnv * env, jobject obj) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return UNKNOWN_VALUE;
  return settings->page_height;
}

JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1SetPageHeight
(JNIEnv * env, jobject obj, jint page_height) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return;
  settings->page_height = page_height;
}


JNIEXPORT jdouble JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1GetMarginTop
(JNIEnv * env, jobject obj) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return UNKNOWN_VALUE;
  return settings->margin_top;
}

JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1SetMarginTop
(JNIEnv * env, jobject obj, jdouble margin_top) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return;
  settings->margin_top = margin_top;
}

JNIEXPORT jdouble JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1GetMarginBottom
(JNIEnv * env, jobject obj) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return UNKNOWN_VALUE;
  return settings->margin_bottom;
}

JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1SetMarginBottom
(JNIEnv * env, jobject obj, jdouble margin_bottom) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return;
  settings->margin_bottom = margin_bottom;
}

JNIEXPORT jdouble JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1GetMarginRight
(JNIEnv * env, jobject obj) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return UNKNOWN_VALUE;
  return settings->margin_right;
}

JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1SetMarginRight
(JNIEnv * env, jobject obj, jdouble margin_right) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return;
  settings->margin_right = margin_right;
}

JNIEXPORT jdouble JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1GetMarginLeft
(JNIEnv * env, jobject obj) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return UNKNOWN_VALUE;
  return settings->margin_left;
}

JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1SetMarginLeft
(JNIEnv * env, jobject obj, jdouble margin_left) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return;
  settings->margin_left = margin_left;
}

JNIEXPORT jobject JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1GetMarginType
(JNIEnv * env, jobject obj) {
  jobject result = GetJNIEnumValue(env,
    "org/cef/misc/CefPdfPrintSettings$CefPdfPrintMarginType", "PDF_PRINT_MARGIN_DEFAULT");

  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return result;

  switch (settings->margin_type) {
    JNI_CASE(env, "org/cef/misc/CefPdfPrintSettings$CefPdfPrintMarginType",
      PDF_PRINT_MARGIN_DEFAULT, result);
    JNI_CASE(env, "org/cef/misc/CefPdfPrintSettings$CefPdfPrintMarginType",
      PDF_PRINT_MARGIN_NONE, result);
    JNI_CASE(env, "org/cef/misc/CefPdfPrintSettings$CefPdfPrintMarginType",
      PDF_PRINT_MARGIN_MINIMUM, result);
    JNI_CASE(env, "org/cef/misc/CefPdfPrintSettings$CefPdfPrintMarginType",
      PDF_PRINT_MARGIN_CUSTOM, result);
  }
  return result;
}

JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1SetMarginType
(JNIEnv * env, jobject obj, jobject jPdfPrintMarginType) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return;

  cef_pdf_print_margin_type_t mode;
  if (IsJNIEnumValue(env, jPdfPrintMarginType,
    "org/cef/misc/CefPdfPrintSettings$CefPdfPrintMarginType",
    "PDF_PRINT_MARGIN_DEFAULT")) {
    mode = PDF_PRINT_MARGIN_DEFAULT;
  }
  else if (IsJNIEnumValue(env, jPdfPrintMarginType,
    "org/cef/misc/CefPdfPrintSettings$CefPdfPrintMarginType",
    "PDF_PRINT_MARGIN_NONE")) {
    mode = PDF_PRINT_MARGIN_NONE;
  }
  else if (IsJNIEnumValue(env, jPdfPrintMarginType,
    "org/cef/misc/CefPdfPrintSettings$CefPdfPrintMarginType",
    "PDF_PRINT_MARGIN_MINIMUM")) {
    mode = PDF_PRINT_MARGIN_MINIMUM;
  }
  else if (IsJNIEnumValue(env, jPdfPrintMarginType,
    "org/cef/misc/CefPdfPrintSettings$CefPdfPrintMarginType",
    "PDF_PRINT_MARGIN_CUSTOM")) {
    mode = PDF_PRINT_MARGIN_CUSTOM;
  }
  
  settings->margin_type = mode;
}

JNIEXPORT jint JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1GetHeaderFooterEnabled
(JNIEnv * env, jobject obj) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return UNKNOWN_VALUE;
  return settings->header_footer_enabled;
}

JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1SetHeaderFooterEnabled
(JNIEnv * env, jobject obj, jint header_footer_enabled) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return;
  settings->header_footer_enabled = header_footer_enabled;
}

JNIEXPORT jint JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1GetSelectionOnly
(JNIEnv * env, jobject obj) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return UNKNOWN_VALUE;
  return settings->selection_only;
}

JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1SetSelectionOnly
(JNIEnv * env, jobject obj, jint selection_only) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return;
  settings->selection_only = selection_only;
}

JNIEXPORT jint JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1GetLandscape
(JNIEnv * env, jobject obj) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return UNKNOWN_VALUE;
  return settings->landscape;
}

JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1SetLandscape
(JNIEnv * env, jobject obj, jint landscape) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return;
  settings->landscape = landscape;
}

JNIEXPORT jint JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1GetBackgroundsEnabled
(JNIEnv * env, jobject obj) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return UNKNOWN_VALUE;
  return settings->backgrounds_enabled;
}

JNIEXPORT void JNICALL Java_org_cef_misc_CefPdfPrintSettings_1N_N_1SetBackgroundsEnabled
(JNIEnv * env, jobject obj, jint backgrounds_enabled) {
  CefPdfPrintSettings* settings =
    GetCefFromJNIObject<CefPdfPrintSettings>(env, obj, "CefPdfPrintSettings");
  if (!settings)
    return;
  settings->backgrounds_enabled = backgrounds_enabled;
}
