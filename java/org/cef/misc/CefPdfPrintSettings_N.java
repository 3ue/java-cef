package org.cef.misc;

import org.cef.callback.CefNative;

public class CefPdfPrintSettings_N extends CefPdfPrintSettings implements CefNative {

  // Used internally to store a pointer to the CEF object.
  private long N_CefHandle = 0;
  private int UNKNOWN_VALUE = -1;

  @Override
  public void setNativeRef(String identifer, long nativeRef) {
    N_CefHandle = nativeRef;
  }

  @Override
  public long getNativeRef(String identifer) {
    return N_CefHandle;
  }

  private CefPdfPrintSettings_N() {
    super();
  }

  public static final CefPdfPrintSettings createNative() {
    CefPdfPrintSettings_N result = new CefPdfPrintSettings_N();
    try {
      result.N_CefPdfPrintSettings_CTOR();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    if (result.N_CefHandle == 0)
      return null;
    return result;
  }

  @Override
  protected void finalize() throws Throwable {
    try {
      N_CefPdfPrintSettings_DTOR();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    } finally {
      super.finalize();
    }
  }

  @Override
  public String getHeaderFooterTitle() {
    try {
      return N_GetHeaderFooterTitle();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    return null;
  }

  @Override
  public void setHeaderFooterTitle(String headerFooterTitle) {
    try {
      N_SetHeaderFooterTitle(headerFooterTitle);
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
  }

  @Override
  public String getHeaderFooterUrl() {
    try {
      return N_GetHeaderFooterUrl();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    return null;
  }

  @Override
  public void setHeaderFooterUrl(String headerFooterUrl) {
    try {
      N_SetHeaderFooterUrl(headerFooterUrl);
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
  }

  @Override
  public int getPageWidth() {
    try {
      return N_GetPageWidth();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    return UNKNOWN_VALUE;
  }

  @Override
  public void setPageWidth(int pageWidth) {
    try {
      N_SetPageWidth(pageWidth);
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
  }

  @Override
  public int getPageHeigth() {
    try {
      return N_GetPageHeigth();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    return UNKNOWN_VALUE;
  }

  @Override
  public void setPageHeight(int pageHeight) {
    try {
      N_SetPageHeight(pageHeight);
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }

  }

  @Override
  public double getMarginTop() {
    try {
      return N_GetMarginTop();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    return UNKNOWN_VALUE;
  }

  @Override
  public void setMarginTop(double marginTop) {
    try {
      N_SetMarginTop(marginTop);
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }

  }

  @Override
  public double getMarginBottom() {
    try {
      return N_GetMarginBottom();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    return UNKNOWN_VALUE;
  }

  @Override
  public void setMarginBottom(double marginBottom) {
    try {
      N_SetMarginBottom(marginBottom);
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }

  }

  @Override
  public double getMarginRight() {
    try {
      return N_GetMarginRight();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    return UNKNOWN_VALUE;
  }

  @Override
  public void setMarginRight(double marginRight) {
    try {
      N_SetMarginRight(marginRight);
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }

  }

  @Override
  public double getMarginLeft() {
    try {
      return N_GetMarginLeft();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    return UNKNOWN_VALUE;
  }

  @Override
  public void setMarginLeft(double marginLeft) {
    try {
      N_SetMarginLeft(marginLeft);
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }

  }

  @Override
  public CefPdfPrintMarginType getMarginType() {
    try {
      return N_GetMarginType();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    return CefPdfPrintMarginType.PDF_PRINT_MARGIN_DEFAULT;
  }

  @Override
  public void setMarginType(CefPdfPrintMarginType marginType) {
    try {
      N_SetMarginType(marginType);
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }

  }

  @Override
  public int getHeaderFooterEnabled() {
    try {
      return N_GetHeaderFooterEnabled();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    return UNKNOWN_VALUE;
  }

  @Override
  public void setHeaderFooterEnabled(int headerFooterEnabled) {
    try {
      N_SetHeaderFooterEnabled(headerFooterEnabled);
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }

  }

  @Override
  public int getSelectionOnly() {
    try {
      return N_GetSelectionOnly();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    return UNKNOWN_VALUE;
  }

  @Override
  public void setSelectionOnly(int selectionOnly) {
    try {
      N_SetSelectionOnly(selectionOnly);
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }

  }

  @Override
  public int getLandscape() {
    try {
      return N_GetLandscape();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    return UNKNOWN_VALUE;
  }

  @Override
  public void setLandscape(int landscape) {
    try {
      N_SetLandscape(landscape);
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }

  }

  @Override
  public int getBackgroundsEnabled() {
    try {
      return N_GetBackgroundsEnabled();
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }
    return UNKNOWN_VALUE;
  }

  @Override
  public void setBackgroundsEnabled(int backgroundsEnabled) {
    try {
      N_SetBackgroundsEnabled(backgroundsEnabled);
    } catch (UnsatisfiedLinkError ule) {
      ule.printStackTrace();
    }

  }

  private final native void N_CefPdfPrintSettings_CTOR();

  private final native void N_CefPdfPrintSettings_DTOR();

  private final native String N_GetHeaderFooterTitle();

  private final native void N_SetHeaderFooterTitle(String headerFooterTitle);

  private final native String N_GetHeaderFooterUrl();

  private final native void N_SetHeaderFooterUrl(String headerFooterUrl);

  private final native int N_GetPageWidth();

  private final native void N_SetPageWidth(int pageWidth);

  private final native int N_GetPageHeigth();

  private final native void N_SetPageHeight(int pageHeight);

  private final native double N_GetMarginTop();

  private final native void N_SetMarginTop(double marginTop);

  private final native double N_GetMarginBottom();

  private final native void N_SetMarginBottom(double marginBottom);

  private final native double N_GetMarginRight();

  private final native void N_SetMarginRight(double marginRight);

  private final native double N_GetMarginLeft();

  private final native void N_SetMarginLeft(double marginLeft);

  private final native CefPdfPrintMarginType N_GetMarginType();

  private final native void N_SetMarginType(CefPdfPrintMarginType marginType);

  private final native int N_GetHeaderFooterEnabled();

  private final native void N_SetHeaderFooterEnabled(int headerFooterEnabled);

  private final native int N_GetSelectionOnly();

  private final native void N_SetSelectionOnly(int selectionOnly);

  private final native int N_GetLandscape();

  private final native void N_SetLandscape(int landscape);

  private final native int N_GetBackgroundsEnabled();

  private final native void N_SetBackgroundsEnabled(int backgroundsEnabled);

}
