package org.cef.misc;

public abstract class CefPdfPrintSettings {

	/**
	 * Margin type for PDF printing.
	 *
	 */
  public enum CefPdfPrintMarginType {
		/**
		 * Default margins.
		 */
		PDF_PRINT_MARGIN_DEFAULT,

		/**
		 * No margins.
		 */
		PDF_PRINT_MARGIN_NONE,

		/**
		 * Minimum margins.
		 */
		PDF_PRINT_MARGIN_MINIMUM,

		/**
		 * Custom margins using the |margin_*| values from
		 * cef_pdf_print_settings_t.
		 */
		PDF_PRINT_MARGIN_CUSTOM,
	};

	/**
	 * Page title to display in the header. Only used if
	 * |header_footer_enabled|is set to true (1).
	 * 
	 * @return
	 */
	abstract public String getHeaderFooterTitle();

	/**
	 * Page title to display in the header. Only used if
	 * |header_footer_enabled|is set to true (1).
	 * 
	 */
	abstract public void setHeaderFooterTitle(String headerFooterTitle);

	/**
	 * URL to display in the footer. Only used if |header_footer_enabled| is set
	 * to true (1).
	 * 
	 * @return
	 */
	abstract public String getHeaderFooterUrl();

	/**
	 * URL to display in the footer. Only used if |header_footer_enabled| is set
	 * to true (1).
	 * 
	 */
	abstract public void setHeaderFooterUrl(String headerFooterUrl);

	/**
	 * Output page size in microns. If either of these values is less than or
	 * equal to zero then the default paper size (A4) will be used.
	 * 
	 * @return
	 */
	abstract public int getPageWidth();

	/**
	 * Output page size in microns. If either of these values is less than or
	 * equal to zero then the default paper size (A4) will be used.
	 * 
	 */
	abstract public void setPageWidth(int pageWidth);

	/**
	 * Output page size in microns. If either of these values is less than or
	 * equal to zero then the default paper size (A4) will be used.
	 * 
	 * @return
	 */
	abstract public int getPageHeigth();

	/**
	 * Output page size in microns. If either of these values is less than or
	 * equal to zero then the default paper size (A4) will be used.
	 * 
	 */
	abstract public void setPageHeight(int pageHeight);

	/**
	 * Margins in millimeters. Only used if |margin_type| is set to
	 * PDF_PRINT_MARGIN_CUSTOM.
	 * 
	 * @return
	 */
	abstract public double getMarginTop();

	/**
	 * Margins in millimeters. Only used if |margin_type| is set to
	 * PDF_PRINT_MARGIN_CUSTOM.
	 * 
	 */
	abstract public void setMarginTop(double marginTop);

	/**
	 * Margins in millimeters. Only used if |margin_type| is set to
	 * PDF_PRINT_MARGIN_CUSTOM.
	 * 
	 * @return
	 */
	abstract public double getMarginBottom();

	/**
	 * Margins in millimeters. Only used if |margin_type| is set to
	 * PDF_PRINT_MARGIN_CUSTOM.
	 * 
	 */
	abstract public void setMarginBottom(double marginBottom);

	/**
	 * Margins in millimeters. Only used if |margin_type| is set to
	 * PDF_PRINT_MARGIN_CUSTOM.
	 * 
	 * @return
	 */
	abstract public double getMarginRight();

	/**
	 * Margins in millimeters. Only used if |margin_type| is set to
	 * PDF_PRINT_MARGIN_CUSTOM.
	 * 
	 */
	abstract public void setMarginRight(double marginRight);

	/**
	 * Margins in millimeters. Only used if |margin_type| is set to
	 * PDF_PRINT_MARGIN_CUSTOM.
	 * 
	 * @return
	 */
	abstract public double getMarginLeft();

	/**
	 * Margins in millimeters. Only used if |margin_type| is set to
	 * PDF_PRINT_MARGIN_CUSTOM.
	 * 
	 */
	abstract public void setMarginLeft(double marginLeft);

	/**
	 * Margin type.
	 * 
	 * @return
	 */
	abstract public CefPdfPrintMarginType getMarginType();

	/**
	 * Margin type.
	 */
	abstract public void setMarginType(CefPdfPrintMarginType marginType);

	/**
	 * Set to true (1) to print headers and footers or false (0) to not print
	 * headers and footers.
	 * 
	 * @return
	 */
	abstract public int getHeaderFooterEnabled();

	/**
	 * Set to true (1) to print headers and footers or false (0) to not print
	 * headers and footers.
	 * 
	 */
	abstract public void setHeaderFooterEnabled(int headerFooterEnabled);

	/**
	 * Set to true (1) to print the selection only or false (0) to print all.
	 * 
	 * @return
	 */
	abstract public int getSelectionOnly();

	/**
	 * Set to true (1) to print the selection only or false (0) to print all.
	 * 
	 */
	abstract public void setSelectionOnly(int selectionOnly);

	/**
	 * Set to true (1) for landscape mode or false (0) for portrait mode.
	 * 
	 * @return
	 */
	abstract public int getLandscape();

	/**
	 * Set to true (1) for landscape mode or false (0) for portrait mode.
	 * 
	 */
  abstract public void setLandscape(int landscape);
	  ///
	  // Set to true (1) to print background graphics or false (0) to not print
	  // background graphics.
	  ///

	/**
	 * Set to true (1) to print background graphics or false (0) to not print
	 * background graphics.
	 * 
	 * @return
	 */
	abstract public int getBackgroundsEnabled();

	/**
	 * Set to true (1) to print background graphics or false (0) to not print
	 * background graphics.
	 * 
	 * @return
	 */
	abstract public void setBackgroundsEnabled(int backgroundsEnabled);
}
